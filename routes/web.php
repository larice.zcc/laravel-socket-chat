<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'ChatController@index');
Route::get('/registerUser', 'ChatController@registerUser');
Route::get('/storeChat', 'ChatController@storeChat');
Route::get('/storess', 'ChatController@storess');