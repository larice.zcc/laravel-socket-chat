<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Laravel Socket.io Chat Example</title>
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
</head>

<body>
  <ul class="pages">

    <li class="chat page">
      <div class="chatArea">
        <ul class="messages"></ul>
      </div>
      <input class="inputMessage" placeholder="Type here..." />
    </li>

    <li class="login page">
	
      <div class="form">
        <h3 class="title">Login With Username</h3>
		<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
        <input class="usernameInput" type="text" maxlength="14"/>
		
      </div>
    </li>
  </ul>
  <script> 
  var base_url = '<?php echo url('/registerUser'); ?>';
  var base_url1 = '<?php echo url('/storeChat'); ?>';
  var base_url2 = '<?php echo url('/storess'); ?>';
  </script>
  <script src="{{ URL::asset('js/app.js') }}"></script>
</body>

</html>